import DataProviders.EmployeeDataProvider;
import DataProviders.EmployerDataProvider;
import DataProviders.RegulationsDataProvider;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class EmployerRegisterComponentTest extends BaseTest {

    @DataProvider(name = "lang")
    public static Object[][] language() {

        return new Object[][]{
                {"pl"},
                {"ua"},
        };
    }

    @DataProvider(name = "shouldCourtesyFormChangedToMale")
    public static Object[][] shouldCourtesyFormChangedToMale() {

        return new Object[][]{
                {"pl", "Pan"},
                {"ua", "Пан"}
        };
    }

    @DataProvider(name = "shouldCourtesyFormChangedToFemale")
    public static Object[][] shouldCourtesyFormChangedToFemale() {

        return new Object[][]{
                {"pl", "Pani"},
                {"ua", "Пані"}
        };
    }

    @DataProvider(name = "shouldRegulationsPageOpenFromALink")
    public static Object[][] shouldRegulationsPageOpenFromALink() {

        return new Object[][]{
                {"pl", RegulationsDataProvider.getExpectedValuePl},
                {"ua", RegulationsDataProvider.getExpectedValueUa}
        };
    }

    @DataProvider(name = "shouldRecruitmentTypeFormChanged")
    public static Object[][] shouldRecruitmentTypeFormChanged() {

        return EmployerDataProvider.getListWithValues(3);
    }

    @DataProvider(name = "shouldVoivodeshipChanged")
    public static Object[][] shouldVoivodeshipChanged() {

        return EmployerDataProvider.getVoivodeshipValue();
    }

    @DataProvider(name = "shouldCategoryChanged")
    public static Object[][] shouldCategoryChanged() {

        return EmployerDataProvider.getCategoryValue();
    }

    @DataProvider(name = "shouldErrorMessagesAppears")
    public static Object[][] shouldErrorMessagesAppears() {

        return new Object[][]{
                {"pl", EmployerDataProvider.getListStringForPl()},
                {"ua", EmployerDataProvider.getListStringForUa()},
        };
    }

    @DataProvider(name = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public static Object[][] shouldInformationPupUpDisplayedAfterHoverOnInformationIcon() {

        return new Object[][]{
                {"pl", EmployerDataProvider.getExpectedInformationValueStringForPl()},
                {"ua", EmployerDataProvider.getExpectedInformationValueStringForUa()}
        };
    }

    @DataProvider
    public static Object[][] shouldValidationEmailMessageAppears() {
        return new Object[][]{
                {"pl", "a", "Minimalna długość adresu e-mail to 4 znaki."},
                {"pl", "aaaa", "Nieprawidłowy format adresu e-mail."},
                {"ua", "a", "Мінімальна довжина адреси електронної пошти - 4 символи."},
                {"ua", "aaaa", "Невірний формат адресу e-mail."},
        };
    }

    @DataProvider
    public static Object[][] shouldPasswordValidationMessageAppears() {
        return new Object[][]{
                {"pl", "a", "Hasło musi zawierać małe i duże litery oraz cyfry."},
                {"ua", "a", "Пароль мусить складатися з малих та великих літер, і цифр."},
        };
    }

    @DataProvider
    public static Object[][] shouldConfirmPasswordValidationMessageAppears() {
        return new Object[][]{
                {"pl", "a1A", "Podane hasła nie są identyczne."},
                {"ua", "a1A", "Вказані паролі не ідентичні."},
        };
    }

    @DataProvider
    public static Object[][] shouldUserNotRegisterWithInvalidEmail() {
        return EmployeeDataProvider.getInvalidEmails();
    }

    @DataProvider
    public static Object[][] shouldPhoneNumberErrorMessageAppears() {
        return new Object[][]{
                {"pl", "1", "Nieprawidłowy format numeru telefonu."},
                {"pl", "1111111111111111111111111111", "Nieprawidłowy format numeru telefonu."},
                {"pl", "aaaa", "Nieprawidłowy format numeru telefonu."},
                {"ua", "1", "Невірний формат номера телефону."},
                {"ua", "1111111111111111111111111111", "Невірний формат номера телефону."},
                {"ua", "aaaa", "Невірний формат номера телефону."},
        };
    }

    @DataProvider
    public static Object[][] shouldNipErrorMessageAppears() {
        return new Object[][]{
                {"pl", "1", "Podano nieprawidłowy numer NIP."},
                {"pl", "111111111111111111111111", "Podano nieprawidłowy numer NIP."},
                {"pl", "a", "Podano nieprawidłowy numer NIP."},
                {"ua", "1", "Вказано невірний номер NIP ((ідентифікаційний код)"},
                {"ua", "111111111111111111111111", "NВказано невірний номер NIP ((ідентифікаційний код)"},
                {"ua", "a", "Вказано невірний номер NIP ((ідентифікаційний код)"},
        };
    }

    @DataProvider
    public static Object[][] shouldRegonErrorMessageAppears() {
        return new Object[][]{
                {"pl", "1", "Podano nieprawidłowy numer REGON."},
                {"pl", "111111111111111111111111", "Podano nieprawidłowy numer REGON."},
                {"pl", "a", "Podano nieprawidłowy numer REGON."},
                {"ua", "1", "Вказано невірний номер REGON (ЄДР)"},
                {"ua", "111111111111111111111111", "Вказано невірний номер REGON (ЄДР)"},
                {"ua", "a", "Вказано невірний номер REGON (ЄДР)"},
        };
    }

    @Test(dataProvider = "shouldErrorMessagesAppears")
    public void shouldErrorMessagesAppears(String lang, ArrayList<String> expectedList) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .submitRegistrationForErrorMessages()
                .assertThatErrorMesagesAppears(expectedList);
    }

    @Test(dataProvider = "shouldCourtesyFormChangedToMale")
    public void shouldCourtesyFormChangedToMale(String lang, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .chooseCourtesyForm("1")
                .assertThatCourtesyFormChanged(expectedValue);
    }

    @Test(dataProvider = "shouldCourtesyFormChangedToFemale")
    public void shouldCourtesyFormChangedToFemale(String lang, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .chooseCourtesyForm("1")
                .chooseCourtesyForm("0")
                .assertThatCourtesyFormChanged(expectedValue);
    }

    @Test(dataProvider = "shouldRecruitmentTypeFormChanged")
    public void shouldRecruitmentTypeFormChanged(String lang, String value, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .chooseRecruitmentType(value)
                .assertThatRecruitmentTypeChanged(expectedValue);
    }


    @Test(dataProvider = "shouldVoivodeshipChanged")
    public void shouldVoivodeshipChanged(String lang, String value, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .chooseVoivodeshipType(value)
                .assertThatVoivodeshipTypeChanged(expectedValue);

    }

    @Test(dataProvider = "shouldCategoryChanged")
    public void shouldCategoryChanged(String lang, String value, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .chooseCategoryType(value)
                .assertThatCategotyTypeChanged(expectedValue);
    }

    @Test(dataProvider = "lang")
    public void shouldCheckLicence(String lang) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .checkLicenseAcceptanceCheckbox()
                .assertThatLicenceCheckboxIsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckAndUncheckedLicence(String lang) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .checkLicenseAcceptanceCheckbox()
                .checkLicenseAcceptanceCheckbox()
                .assertThatLicenceCheckboxIsUchecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckTermOfUse(String lang) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .checkTermsOfUseCheckbox()
                .assertThattermsOfUseCheckboxIsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckAndUncheckedTermOfUse(String lang) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .checkTermsOfUseCheckbox()
                .checkTermsOfUseCheckbox()
                .assertThattermsOfUseCheckboxIsUnchecked();
    }

    @Test(dataProvider = "shouldRegulationsPageOpenFromALink")
    public void shouldRegulationsPageOpenFromALink(String lang, String expectedValue) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .clickRegulationsLink()
                .assertThatRegulationsPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public void shouldInformationPupUpDisplayedAfterHoverOnInformationIcon(String lang, List<String> expectedValue, ITestContext context) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .hoverAndAssertInformationIcons(expectedValue, context);
    }

    @Test(dataProvider = "shouldValidationEmailMessageAppears")
    public void shouldValidationEmailMessageAppears(String lang, String email, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField(email)
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldPasswordValidationMessageAppears")
    public void shouldPasswordValidationMessageAppears(String lang, String password, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField(password)
                .fillConfirmPassword(password)
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldConfirmPasswordValidationMessageAppears")
    public void shouldConfirmPasswordValidationMessageAppears(String lang, String password, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField(password)
                .fillConfirmPassword("a1B")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldUserNotRegisterWithInvalidEmail")
    public void shouldUserNotRegisterWithInvalidEmail(String lang, String email) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField(email)
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatUserNotRegistered();
    }

    @Test(dataProvider = "shouldPhoneNumberErrorMessageAppears")
    public void shouldPhoneNumberErrorMessageAppears(String lang, String phoneNumber, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField(phoneNumber)
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldNipErrorMessageAppears")
    public void shouldNipErrorMessageAppears(String lang, String nip, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField(nip)
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldRegonErrorMessageAppears")
    public void shouldRegonErrorMessageAppears(String lang, String regon, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField(regon)
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField("111222333")
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldPhoneNumberErrorMessageAppears")
    public void shouldCompanyPhoneNumberErrorMessageAppears(String lang, String companyPhoneNumber, String expectedMessage) {
        employerRegisterComponent
                .goToEmployerRegisterPage(lang)
                .fillEmailField("test@test.pl")
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .fillPositionField("C.A.O.")
                .fillNameField("Test")
                .fillSurnameField("Test")
                .fillPhoneNumberField("111222333")
                .chooseRecruitmentType("1")
                .fillNipField("1922247985")
                .fillRegonField("856872208")
                .fillCompanyNameField("Test")
                .fillPostalCodeField("30-400")
                .fillLocalityField("Test")
                .fillAddressField("Test")
                .chooseVoivodeshipType("5")
                .chooseCategoryType("6")
                .fillEmployeeCountField("10")
                .fillCompanyEmailField("test@test.pl")
                .fillCompanyPhoneField(companyPhoneNumber)
                .fillWebsiteField("www.test.pl")
                .fillDescriptionField("Test")
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }
}
