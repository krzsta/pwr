import DataProviders.EmployeeDataProvider;
import DataProviders.RegulationsDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static Helpers.DataTime.getActualTimestamp;

public class EmployeeRegisterComponentTest extends BaseTest {

    @DataProvider(name = "lang")
    public static Object[][] language() {

        return new Object[][]{
                {"pl"},
                {"ua"},
        };
    }

    @DataProvider(name = "shouldGenderChanged")
    public static Object[][] shouldGenderChanged() {

        return new Object[][]{
                {"pl", "mężczyzna"},
                {"ua", "чоловік"},
        };
    }

    @DataProvider(name = "shouldGenderChangedBack")
    public static Object[][] shouldGenderChangedBack() {

        return new Object[][]{
                {"pl", "kobieta"},
                {"ua", "жінка"},
        };
    }

    @DataProvider(name = "shouldErrorMessagesAppears")
    public static Object[][] shouldErrorMessagesAppears() {

        return new Object[][]{
                {"pl", EmployeeDataProvider.getListStringForPl()},
                {"ua", EmployeeDataProvider.getListStringForUa()},
        };
    }

    @DataProvider(name = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public static Object[][] shouldInformationPupUpDisplayedAfterHoverOnInformationIcon() {

        return new Object[][]{
                {"pl", EmployeeDataProvider.getExpectedInformationValueStringForPl()},
                {"ua", EmployeeDataProvider.getExpectedInformationValueStringForUa()}
        };
    }

    @DataProvider(name = "shouldRegulationsPageOpenFromALink")
    public static Object[][] shouldRegulationsPageOpenFromALink() {

        return new Object[][]{
                {"pl", RegulationsDataProvider.getExpectedValuePl},
                {"ua", RegulationsDataProvider.getExpectedValueUa}
        };
    }

    @DataProvider(name = "shouldValidationEmailMessageAppears")
    public static Object[][] shouldValidationEmailMessageAppears() {
        return new Object[][]{
                {"pl", "a", "Minimalna długość adresu e-mail to 4 znaki."},
                {"pl", "aaaa", "Nieprawidłowy format adresu e-mail."},
                {"ua", "a", "Мінімальна довжина адреси електронної пошти - 4 символи."},
                {"ua", "aaaa", "Невірний формат адресу e-mail."},
        };
    }

    @DataProvider(name = "shouldPasswordValidationMessageAppears")
    public static Object[][] shouldPasswordValidationMessageAppears() {
        return new Object[][]{
                {"pl", "a", "Hasło musi zawierać małe i duże litery oraz cyfry."},
                {"ua", "a", "Пароль мусить складатися з малих та великих літер, і цифр."},
        };
    }

    @DataProvider(name = "shouldConfirmPasswordValidationMessageAppears")
    public static Object[][] shouldConfirmPasswordValidationMessageAppears() {
        return new Object[][]{
                {"pl", "a", "Podane hasła nie są identyczne."},
                {"ua", "a", "Вказані паролі не ідентичні."},
        };
    }

    @DataProvider(name = "shouldUserNotRegisterWithInvalidEmail")
    public static Object[][] shouldUserNotRegisterWithInvalidEmail() {
        return EmployeeDataProvider.getInvalidEmails();
    }

    @DataProvider(name = "shouldUserRegister")
    public static Object[][] shouldUserRegister() {
        return new Object[][]{
                {"pl", "×\nSukces!\nZarejestrowano pomyślnie. Na podany adres email została wysłana wiadomość z linkiem aktywacyjnym."},
                {"ua", "×\nУспіх!\nУспішно зареєстровано. На поданий адрес email вислано повідомлення з посиланням для активації."},
        };
    }

    @DataProvider(name = "shouldUserLoginAfterRegister")
    public static Object[][] shouldUserLoginAfterRegister() {
        return new Object[][]{
                {"pl", "pwremails+" + getActualTimestamp() + "@gmail.com", "Admin0981"},
                {"ua", "pwremails+" + (getActualTimestamp() + 1) + "@gmail.com", "Admin0981"},
        };
    }

    @DataProvider(name = "shouldReceiveProperEmailAfterRegister")
    public static Object[][] shouldReceiveProperEmailAfterRegister() {

        ArrayList<String> pl = new ArrayList<>();
        pl.add("ziękujemy za rejestrację w serwisie pracawregionie.pl");
        pl.add("http://www.stage.pracawregionie.pl/user/auth/activate/");

        ArrayList<String> ua = new ArrayList<>();
        ua.add("Дякуємо за реєстрацію на сайті pracawregionie.pl");
        ua.add("http://www.stage.pracawregionie.pl/ua/user/auth/activate/");

        return new Object[][]{
                {"pl", "pwremails+" + getActualTimestamp() + "@gmail.com", "Admin0981", pl},
                {"ua", "pwremails+" + (getActualTimestamp() + 1) + "@gmail.com", "Admin0981", ua},
        };
    }

    @Test(dataProvider = "shouldErrorMessagesAppears")
    public void shouldErrorMessagesAppears(String lang, ArrayList<String> expectedList) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .submitRegistrationForErrorMessages()
                .assertThatErrorMessagesAppears(expectedList);
    }

    @Test(dataProvider = "shouldGenderChanged")
    public void shouldGenderChanged(String lang, String expectedValue) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .chooseGender("1")
                .assertThatGenderChanged(expectedValue);
    }

    @Test(dataProvider = "shouldGenderChangedBack")
    public void shouldGenderChangedBack(String lang, String expectedValue) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .chooseGender("1")
                .chooseGender("0")
                .assertThatGenderChanged(expectedValue);
    }

    @Test(dataProvider = "lang")
    public void shouldCheckCategories(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkAllCategories()
                .assertThatCategoriesChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckCategoriesAndUncheck(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkAllCategories()
                .checkAllCategories()
                .assertThatCategotiesUnchecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckVoivodeships(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkAllVoivodeships()
                .assertThatVodeshipsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckVoivodeshipsAndUncheck(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkAllVoivodeships()
                .checkAllVoivodeships()
                .assertThatVodeshipsUnchecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckLicence(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkLicenceCheckbox()
                .assertThatLicenceCheckboxIsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckAndUncheckLicence(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkLicenceCheckbox()
                .checkLicenceCheckbox()
                .assertThatLicenceCheckboxIsUchecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckTermOfUse(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkTermOfUseCheckbox()
                .assertThatTermOfUseCheckboxIsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckAndUncheckTermOfUse(String lang) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .checkTermOfUseCheckbox()
                .checkTermOfUseCheckbox()
                .assertThatTermOfUseCheckboxIsUnchecked();
    }

    @Test(dataProvider = "shouldRegulationsPageOpenFromALink")
    public void shouldRegulationsPageOpenFromALink(String lang, String expectedValue) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .clikRegulationsLink()
                .assertThatRegulationsPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public void shouldInformationPupUpDisplayedAfterHoverOnInformationIcon(String lang, List<String> expectedValue) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .AssertThatInformationPopUpDisplayed(expectedValue);
    }

    @Test(dataProvider = "shouldValidationEmailMessageAppears")
    public void shouldValidationEmailMessageAppears(String lang, String email, String expectedMessage) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillEmailField(email)
                .submitRegistrationForErrorMessages()
                .assertThatProperEmailMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldPasswordValidationMessageAppears")
    public void shouldPasswordValidationMessageAppears(String lang, String password, String expectedMessage) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillPasswordField(password)
                .submitRegistrationForErrorMessages()
                .assertThatProperPasswordErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldConfirmPasswordValidationMessageAppears")
    public void shouldConfirmPasswordValidationMessageAppears(String lang, String password, String expectedMessage) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillPasswordField(password)
                .submitRegistrationForErrorMessages()
                .assertThatConfirmPasswordErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldUserNotRegisterWithInvalidEmail")
    public void shouldUserNotRegisterWithInvalidEmail(String lang, String email) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillEmailField(email)
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .selectCategory(5)
                .selectVoivodeship(5)
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistrationForErrorMessages()
                .assertThatUserNotRegistered();
    }

    @Test(dataProvider = "shouldUserRegister")
    public void shouldUserRegister(String lang, String expectedMessage) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillEmailField("pwremails+" + getActualTimestamp() + "@gmail.com")
                .fillPasswordField("a1A")
                .fillConfirmPassword("a1A")
                .selectCategory(5)
                .selectVoivodeship(5)
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistration()
                .assertThatUserRegistered(expectedMessage);
    }

    //TODO dokonczyc jak logowanie bedzie dzialac
    @Test(dataProvider = "shouldUserLoginAfterRegister")
    public void shouldUserLoginAfterRegister(String lang, String email, String password) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillEmailField(email)
                .fillPasswordField(password)
                .fillConfirmPassword(password)
                .selectCategory(5)
                .selectVoivodeship(5)
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistration()
                .fillEmailFieldAfterRegister(email)
                .fillPasswordField(password)
                .submitButtonToLogin();
    }

    @Test(dataProvider = "shouldReceiveProperEmailAfterRegister")
    public void shouldReceiveProperEmailAfterRegister(String lang, String email, String password, ArrayList<String> expectedValue) {
        employeeRegisterComponent
                .goToEmployeeRegisterPage(lang)
                .fillEmailField(email)
                .fillPasswordField(password)
                .fillConfirmPassword(password)
                .selectCategory(5)
                .selectVoivodeship(5)
                .checkTermOfUseCheckbox()
                .checkLicenceCheckbox()
                .submitRegistration()
                .assertThatValidationEmailReceived(email, expectedValue);
    }
}
