import DataProviders.ChangePasswordDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ChangePasswordPageTest extends BaseTest {

    @DataProvider(name = "lang")
    public static Object[][] lang() {

        return new Object[][]{
                {"pl"},
                {"ua"},
        };
    }

    @DataProvider(name = "shouldErrorMessagesAppears")
    public static Object[][] shouldErrorMessagesAppears() {

        return ChangePasswordDataProvider.getErrorMessage();
    }

    @Test(dataProvider = "shouldErrorMessagesAppears")
    public void shouldErrorMessagesAppears(String lang, String expetedValue) {
        changePasswordPage
                .goToChangePasswordPage(lang)
                .submitButtonForErrorMessage()
                .assertThatErrorMessageIsDisplayed(expetedValue);
    }

    @Test(dataProvider = "lang")
    public void shouldErrorMessagesClosed(String lang) {
        changePasswordPage
                .goToChangePasswordPage(lang)
                .submitButtonForErrorMessage()
                .clickOnCloseErrorMessageContainerButton()
                .assertThatErrorMessageIsClosed();
    }
}
