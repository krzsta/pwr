import DataProviders.EmployeeDataProvider;
import DataProviders.EmployerDataProvider;
import DataProviders.SmallAdvertisementDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AddContainerComponentTest extends BaseTest {

    @DataProvider(name = "shouldUserRegisterPageOpenFromAddContainer")
    public static Object[][] shouldUserRegisterPageOpenFromAddContainer() {

        return new Object[][]{
                {"pl", EmployeeDataProvider.getExpectedValuePl},
                {"ua", EmployeeDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldCompanyRegisterPageOpenFromAddContainer")
    public static Object[][] shouldCompanyRegisterPageOpenFromAddContainer() {

        return new Object[][]{
                {"pl", EmployerDataProvider.getExpectedValuePl},
                {"ua", EmployerDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldSmallAdvertisementPageOpenFromAddContainer")
    public static Object[][] shouldSmallAdvertisementPageOpenFromAddContainer() {

        return new Object[][]{
                {"pl", SmallAdvertisementDataProvider.getExpectedValuePl},
                {"ua", SmallAdvertisementDataProvider.getExpectedValueUa},
        };
    }

    @Test(dataProvider = "shouldUserRegisterPageOpenFromAddContainer")
    public void shouldUserRegisterPageOpenFromAddContainer(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getAddContainerComponent()
                .clickOnUserRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldCompanyRegisterPageOpenFromAddContainer")
    public void shouldCompanyRegisterPageOpenFromAddContainer(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getAddContainerComponent()
                .clickOnCompanyRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldSmallAdvertisementPageOpenFromAddContainer")
    public void shouldSmallAdvertisementPageOpenFromAddContainer(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getAddContainerComponent()
                .clickOnSmallAdvertisementLink()
                .assertThatPageOpened(expectedValue);
    }
}
