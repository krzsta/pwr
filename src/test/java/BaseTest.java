import Core.EmailExecutor;
import Core.WebDriverFactory;
import Pages.PagesAfterLogin.MainPageAfterLogin;
import Pages.PagesBeforeLogin.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import static Core.DbExecutor.updateDatabase;
import static Core.EmailExecutor.deleteAllEmails;

public class BaseTest {
    private WebDriver driver;
    private WebDriverFactory webDriverFactory;
    MainPage mainPage;
    TopNavigationComponent topNavigationComponent;
    EmployeeRegisterComponent employeeRegisterComponent;
    EmployerRegisterComponent employerRegisterComponent;
    AddContainerComponent addContainerComponent;
    LoginPopUpPage loginPopUpPage;
    SmallAdvertisementComponent smallAdvertisementComponent;
    ChangePasswordPage changePasswordPage;
    LoginPage loginPage;
    MainPageAfterLogin mainPageAfterLogin;

    @BeforeMethod
    @Parameters(("browser"))
    public void setUp(@Optional("firefox") String browser) {
        webDriverFactory = new WebDriverFactory();
        driver = webDriverFactory.getBaseWebDriver(browser);
        driver.manage().deleteAllCookies();

        mainPage = new MainPage(driver);
        topNavigationComponent = new TopNavigationComponent(driver);
        employeeRegisterComponent = new EmployeeRegisterComponent(driver);
        employerRegisterComponent = new EmployerRegisterComponent(driver);
        addContainerComponent = new AddContainerComponent(driver);
        loginPopUpPage = new LoginPopUpPage(driver);
        smallAdvertisementComponent = new SmallAdvertisementComponent(driver);
        changePasswordPage = new ChangePasswordPage(driver);
        loginPage = new LoginPage(driver);

        mainPageAfterLogin = new MainPageAfterLogin(driver);

        deleteAllEmails();
    }

    @AfterMethod
    public void close() {
        driver.quit();
        updateDatabase("DELETE FROM announcements WHERE email = '" + EmailExecutor.properEmail + "'");
        updateDatabase("DELETE FROM users WHERE email like 'pwremails+%'");
    }
}
