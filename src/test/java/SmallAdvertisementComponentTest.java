import Core.EmailExecutor;
import DataProviders.EmployeeDataProvider;
import DataProviders.SmallAdvertisementDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static Helpers.DataTime.getActualTimestamp;

public class SmallAdvertisementComponentTest extends BaseTest {

    @DataProvider(name = "shouldSuccessAdvertisementAppearsOnMainPageAfterSaveAdvertisement")
    public static Object[][] shouldSuccessAdvertisementAppearsOnMainPageAfterSaveAdvertisement() {

        return new Object[][]{
                {"pl", "Test " + getActualTimestamp()},
                {"ua", "Test " + getActualTimestamp()}
        };
    }

    @DataProvider(name = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public static Object[][] shouldInformationPupUpDisplayedAfterHoverOnInformationIcon() {

        return new Object[][]{
                {"pl", SmallAdvertisementDataProvider.getExpectedInformationValueStringForPl()},
                {"ua", SmallAdvertisementDataProvider.getExpectedInformationValueStringForUa()}
        };
    }

    @DataProvider(name = "shouldErrorMessagesAppears")
    public static Object[][] shouldErrorMessagesAppears() {

        return new Object[][]{
                {"pl", SmallAdvertisementDataProvider.getListStringForPl()},
                {"ua", SmallAdvertisementDataProvider.getListStringForUa()}
        };
    }

    @DataProvider(name = "shouldNotSaveAdvertisementWithInvalidEmail")
    public static Object[][] shouldNotSaveAdvertisementWithInvalidEmail() {
        return EmployeeDataProvider.getInvalidEmails();
    }

    @DataProvider(name = "shouldPhoneNumberErrorMessageAppears")
    public static Object[][] shouldPhoneNumberErrorMessageAppears() {
        return new Object[][]{
                {"pl", "1", "Nieprawidłowy format numeru telefonu."},
                {"pl", "1111111111111111111111111111", "Nieprawidłowy format numeru telefonu."},
                {"pl", "aaaa", "Nieprawidłowy format numeru telefonu."},
                {"ua", "1", "Невірний формат номера телефону."},
                {"ua", "1111111111111111111111111111", "Невірний формат номера телефону."},
                {"ua", "aaaa", "Невірний формат номера телефону."},
        };
    }

    @DataProvider(name = "shouldSuccessMessagesAppears")
    public static Object[][] shouldSuccessMessagesAppears() {

        return new Object[][]{
                {"pl", "×\nSukces!\nZapisano pomyślnie. Ogłoszenie będzie widoczne na stronie po akceptacji przez administratora."},
                {"ua", "x\nУспіх!\nУспішно збережено. Оголошення буде показане на сайті після затвердження адміністратором."}
        };
    }

    @DataProvider(name = "shouldValidationEmailReceived")
    public static Object[][] shouldValidationEmailReceived() {

        return new Object[][]{
                {"pl", "Test " + getActualTimestamp()},
//                {"ua", "Test " + getActualTimestamp()}
        };
    }

    @Test(dataProvider = "shouldInformationPupUpDisplayedAfterHoverOnInformationIcon")
    public void shouldInformationPupUpDisplayedAfterHoverOnInformationIcon(String lang, ArrayList<String> expectedList) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .AssertThatInformationPopUpDisplayed(expectedList);
    }

    @Test(dataProvider = "shouldErrorMessagesAppears")
    public void shouldErrorMessagesAppears(String lang, ArrayList<String> expectedList) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .submitSmallAdvertisementForErrorMessages()
                .assertThatErrorMessagesAppears(expectedList);
    }

    @Test(dataProvider = "shouldNotSaveAdvertisementWithInvalidEmail")
    public void shouldNotSaveAdvertisementWithInvalidEmail(String lang, String email) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .fillTitleField("Test")
                .fillEmailField(email)
                .fillPhoneNumberField("111222333")
                .fillContentField("Test")
                .submitSmallAdvertisementForErrorMessages()
                .assertThatNotSaveAdvertisement();
    }

    @Test(dataProvider = "shouldPhoneNumberErrorMessageAppears")
    public void shouldPhoneNumberErrorMessageAppears(String lang, String phoneNumber, String expectedMessage) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .fillTitleField("Test")
                .fillEmailField("test@test.pl")
                .fillPhoneNumberField(phoneNumber)
                .fillContentField("Test")
                .submitSmallAdvertisementForErrorMessages()
                .assertThatCorrectErrorMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldSuccessMessagesAppears")
    public void shouldSuccessMessagesAppearsAfterSaveAdvertisement(String lang, String expectedMessage) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .fillTitleField("Test")
                .fillEmailField(EmailExecutor.properEmail)
                .fillPhoneNumberField("111222333")
                .fillContentField("Test")
                .submitSmallAdvertisement()
                .assertThatAdvertisementAddedMessageAppears(expectedMessage);
    }

    @Test(dataProvider = "shouldSuccessAdvertisementAppearsOnMainPageAfterSaveAdvertisement")
    public void shouldSuccessAdvertisementAppearsOnMainPageAfterSaveAdvertisement(String lang, String title) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .fillTitleField(title)
                .fillEmailField(EmailExecutor.properEmail)
                .fillPhoneNumberField("111222333")
                .fillContentField("Test Test Test")
                .submitSmallAdvertisement()
                .assertThatAdvertisementAppearsOnMainPage(title);
    }

    @Test(dataProvider = "shouldValidationEmailReceived")
    public void shouldValidationEmailReceived(String lang, String title) {
        smallAdvertisementComponent
                .goToSmallAdvertisementComponent(lang)
                .fillTitleField(title)
                .fillEmailField(EmailExecutor.properEmail)
                .fillPhoneNumberField("111222333")
                .fillContentField("Test Test Test")
                .submitSmallAdvertisement()
                .assertThatValidationEmailIsPresent(title);
    }
}
