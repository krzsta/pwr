import DataProviders.ChangePasswordDataProvider;
import DataProviders.EmployeeDataProvider;
import DataProviders.EmployerDataProvider;
import DataProviders.LoginPopUpDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginPopUpPageTest extends BaseTest {

    @DataProvider(name = "lang")
    public static Object[][] language() {

        return new Object[][]{
                {"pl"},
                {"ua"},
        };
    }

    @DataProvider(name = "shouldErrorMessageAppears")
    public static Object[][] shouldErrorMessageAppears() {

        return new Object[][]{
                {"pl", LoginPopUpDataProvider.getExpectedValuePl},
                {"ua", LoginPopUpDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldUserRegisterPageOpenFromLink")
    public static Object[][] shouldUserRegisterPageOpenFromLink() {

        return new Object[][]{
                {"pl", EmployeeDataProvider.getExpectedValuePl},
                {"ua", EmployeeDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldCompanyRegisterPageOpenFromLink")
    public static Object[][] shouldCompanyRegisterPageOpenFromLink() {

        return new Object[][]{
                {"pl", EmployerDataProvider.getExpectedValuePl},
                {"ua", EmployerDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldRemindPasswordPageOpenFromLink")
    public static Object[][] shouldRemindPasswordPageOpenFromLink() {

        return new Object[][]{
                {"pl", ChangePasswordDataProvider.getExpectedValuePl},
                {"ua", ChangePasswordDataProvider.getExpectedValueUa},
        };
    }

    @Test(dataProvider = "lang")
    public void shouldCloseRegisterPageByCloseButton(String lang) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .clickOnCloseLoginPopUpPageButton()
                .assertThatLoginPopUpPageClosed();
    }

    @Test(dataProvider = "shouldUserRegisterPageOpenFromLink")
    public void shouldUserRegisterPageOpenFromLink(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .clickOnUserRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldCompanyRegisterPageOpenFromLink")
    public void shouldCompanyRegisterPageOpenFromLink(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .clickOnCompanyRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "lang")
    public void shouldCheckedRemember(String lang) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .checkRememberCheckbox()
                .assertThatRememberIsChecked();
    }

    @Test(dataProvider = "lang")
    public void shouldCheckAndUncheckedRemember(String lang) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .checkRememberCheckbox()
                .checkRememberCheckbox()
                .assertThatRememberIsUnchecked();
    }

    @Test(dataProvider = "shouldRemindPasswordPageOpenFromLink")
    public void shouldRemindPasswordPageOpenFromLink(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .clickOnChangePasswordLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldErrorMessageAppears")
    public void shouldErrorMessageAppears(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .clickOnSubmitButton()
                .assertThatErrorMessageAppears(expectedValue);
    }
}
