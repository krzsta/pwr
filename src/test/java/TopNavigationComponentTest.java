import DataProviders.EmployeeDataProvider;
import DataProviders.EmployerDataProvider;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TopNavigationComponentTest extends BaseTest {

    @DataProvider(name = "lang")
    public static Object[][] language() {

        return new Object[][]{
                {"pl"},
                {"ua"},
        };
    }

    @DataProvider(name = "shouldUserRegisterPageOpenFromDropdownMenu")
    public static Object[][] shouldUserRegisterPageOpenFromDropdownMenu() {

        return new Object[][]{
                {"pl", EmployeeDataProvider.getExpectedValuePl},
                {"ua", EmployeeDataProvider.getExpectedValueUa},
        };
    }

    @DataProvider(name = "shouldCompanyRegisterPageOpenFromDropdownMenu")
    public static Object[][] shouldCompanyRegisterPageOpenFromDropdownMenu() {

        return new Object[][]{
                {"pl", EmployerDataProvider.getExpectedValuePl},
                {"ua", EmployerDataProvider.getExpectedValueUa},
        };
    }

    @Test(dataProvider = "shouldUserRegisterPageOpenFromDropdownMenu")
    public void shouldUserRegisterPageOpenFromDropdownMenu(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnUserRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "shouldCompanyRegisterPageOpenFromDropdownMenu")
    public void shouldCompanyRegisterPageOpenFromDropdownMenu(String lang, String expectedValue) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnCompanyRegisterLink()
                .assertThatPageOpened(expectedValue);
    }

    @Test(dataProvider = "lang")
    public void shouldPopUpLoginPageOpenFromDropdownMenu(String lang) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnDropdownMenu()
                .clickOnLoginLink()
                .assertThatPopUpLoginComponentAppears();
    }

    @Test(dataProvider = "lang")
    public void shouldSwitchToProperLanguageOnMainPage(String lang) {
        mainPage
                .goToMainPage(lang)
                .getTopNavigationComponent()
                .clickOnUkFlag()
                .assertThatSwitchedToUkrainianLanguage();

        mainPage
                .getTopNavigationComponent()
                .clickOnPlFlag()
                .assertThatSwitchedToPolishLanguage();
    }
}
