package DataProviders;

import java.util.ArrayList;

public class EmployerDataProvider {

    public static String getExpectedValuePl = "Rejestracja pracodawcy:";
    public static String getExpectedValueUa = "Реєстрація роботодавця:";

    public static Object[][] getListWithValues(int value) {

        Object[] recruitmentTypePl = {"agencja pośrednictwa pracy", "na potrzeby własnej firmy", "inne"};
        Object[] recruitmentTypeUa = {"агенство посередництва праці", "на потреби власної компанії", "інші"};

        Object[][] objects = new String[value * 2][3];

        for (int i = 0; i < value; i++) {
            objects[i][0] = "pl";
            objects[i][1] = Integer.toString(i + 1);
            objects[i][2] = recruitmentTypePl[i];

            objects[i + value][0] = "ua";
            objects[i + value][1] = Integer.toString(i + 1);
            objects[i + value][2] = recruitmentTypeUa[i];
        }

        return objects;
    }

    public static Object[][] getVoivodeshipValue() {

        String[] valuesTab = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "18", "19"};

        String[] voivodeshipPl = {"dolnośląskie", "kujawsko-pomorskie", "lubelskie", "lubuskie", "łódzkie", "małopolskie", "mazowieckie", "opolskie", "podkarpackie", "podlaskie", "pomorskie", "śląskie",
                "świętokrzyskie", "warmińsko-mazurskie", "wielkopolskie", "zachodniopomorskie", "zagranica", "cały kraj"};

        String[] voivodeshipUa = {"dolnośląskie", "kujawsko-pomorskie", "lubelskie", "lubuskie", "łódzkie", "małopolskie", "mazowieckie", "opolskie", "podkarpackie", "podlaskie", "pomorskie", "śląskie",
                "świętokrzyskie", "warmińsko-mazurskie", "wielkopolskie", "zachodniopomorskie", "закордон", "територія цілої країні"};

        Object[][] objects = new String[valuesTab.length * 2][3];

        for (int i = 0; i < valuesTab.length; i++) {
            objects[i][0] = "pl";
            objects[i][1] = valuesTab[i];
            objects[i][2] = voivodeshipPl[i];

            objects[i + valuesTab.length][0] = "ua";
            objects[i + valuesTab.length][1] = valuesTab[i];
            objects[i + valuesTab.length][2] = voivodeshipUa[i];
        }

        return objects;
    }

    public static Object[][] getCategoryValue() {

        String[] valuesTab = {"6", "7", "8", "9", "10", "13", "14", "15", "16", "17", "19", "20", "21", "22", "23", "29", "30", "31", "32", "33", "34", "35",
                "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52"};

        String[] categoryPl = {"Bankowość / Usługi finansowe", "Budownictwo", "Doradztwo / Konsulting", "Edukacja/Szkolenia", "Farmacja/Laboratorium", "Human Resources", "Instalacje/Utrzymanie/Serwis",
                "Internet/E-Commerce", "Inżynieria", "IT - Bazy danych", "IT - Hardware/Information systems", "IT - Konsulting", "Kontrola jakości", "IT - Programowanie/Analizy", "IT - Project Management",
                "Inna", "Administracja biurowa", "Badania i rozwój", "BHP/Ochrona środowiska", "Call center", "Energetyka", "Finanse/Ekonomia", "Franczyzna/Własny biznes", "Hotelarstwo/Gastronomia/Turystyka",
                "Zdrowie/Uroda/Rekreacja", "Zakupy", "Ubezpieczenia", "Transport/Spedycja", "Sprzedaż", "Sektor publiczny", "Reklama/Grafika/Fotografia", "Public relations", "Produkcja", "Prawo", "Praca fizyczna",
                "Obsługa klienta", "Nieruchomości", "Media/Sztuka/Rozrywka", "Marketing"};

        String[] categoryUa = {"Банківська справа/ Фінансові послуги", "Будівництво", "Консалтинг", "Освіта/Навчання", "Фармація/Лабораторія", "Людські ресурси HR", "Монтаж/Обслуговування/Сервіс",
                "Інтернет/Електронна комерція", "Інжиніринг", "IT - Бази даних", "IT - обладнання / інформаційні системи", "IT - Консалтинг", "Контроль якості", "ІТ - програмування / аналіз", "ІТ - управління проектами",
                "Інша", "Офісна адміністрація/Секретаріат", "Дослідження та розвиток", "Охорона праці/ Охорона навколишнього середовища", "Кол-центр", "Енергетика", "Фінанси/Економіка", "Франциза/Власний бізнес",
                "Готельний бізнес/Ресторанний бізнес/Туризм", "Здоров'я/Краса/Відпочинок", "Покупки", "Страхування", "Транпорт/Експедиція", "Продажі", "Державний сектор", "Реклама/Графіка/Фотографія",
                "Зв'язки з громадськістю PR", "Виробництво", "Право", "Праця фізична", "Обслуговування клієнтів", "Нерухомість", "Медіа / Мистецтво / Розваги", "Маркетинг"};

        Object[][] objects = new String[valuesTab.length * 2][3];

        for (int i = 0; i < valuesTab.length; i++) {
            objects[i][0] = "pl";
            objects[i][1] = valuesTab[i];
            objects[i][2] = categoryPl[i];

            objects[i + valuesTab.length][0] = "ua";
            objects[i + valuesTab.length][1] = valuesTab[i];
            objects[i + valuesTab.length][2] = categoryUa[i];
        }

        return objects;
    }

    public static ArrayList<String> getListStringForPl() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Nie podano adresu e-mail.");
        list.add("Nie wprowadzono hasła.");
        list.add("Nie podano stanowiska.");
        list.add("Nie podano imienia.");
        list.add("Nie podano nazwiska.");
        list.add("Nie podano telefonu kontaktowego.");
        list.add("Nie wybrano typu rekrutacji.");
        list.add("Nie podano numeru NIP.");
        list.add("Nie podano nazwy firmy.");
        list.add("Branża musi zostać wybrana.");
        list.add("Nie podano ilości pracowników.");
        list.add("Regulamin musi zostać zaakceptowany.");

        return list;
    }

    public static ArrayList<String> getListStringForUa() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Не вказано адрес e-mail.");
        list.add("Не введений пароль.");
        list.add("Не вказано посаду.");
        list.add("Не вказане ім'я");
        list.add("Не вказане прізвище.");
        list.add("Не вказано контактний номер телефону.");
        list.add("Не вибрано тип набору.");
        list.add("Не вказано номер NIP (ідентифікаційний код)");
        list.add("Не вказано назву фірми.");
        list.add("Потрібно вибрати галузь.");
        list.add("Не вказано кількість працівників.");
        list.add("Мусиш погодитись з Правилами сайту.");

        return list;
    }

    public static ArrayList<String> getExpectedInformationValueStringForPl() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Stanowisko zajmowane w firmie przez osobę odpowiedzialną za obsługę konta.");
        list.add("W przypadku numerów stacjonarnych należy pamiętać o podaniu numeru kierunkowego.");
        list.add("Ta informacja pozwoli nam lepiej dostosować obsługę do Państwa zapotrzebowania.");
        list.add("Przybliżona ilość pracowników, która pozwoli określić wielkość firmy.");
        list.add("W przypadku numerów stacjonarnych należy pamiętać o podaniu numeru kierunkowego.");
        list.add("Opcjonalny adres strony internetowej firmy.");
        list.add("Opcjonalny opis działalności.");
        list.add("Bez tej zgody nie będziemy mogli przesyłać Państwu drogą mailową informacji na przykład o promocjach.");
        list.add("Akceptacja postanowień regulaminu uprawnia do korzystania z serwisu.");

        return list;
    }

    public static ArrayList<String> getExpectedInformationValueStringForUa() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Посада,яку займає особа, відповідальна за обслуговування аккаунту в компанії.");
        list.add("У випадку стаціонарних номерів, будь ласка, не забудьте ввести код міста.");
        list.add("Ця інформація дозволить нам краще достосувати обслуговування до Ваших потреб.");
        list.add("Орієнтовна кількість працівників, яка дозволить визначити розмір компанії.");
        list.add("У випадку стаціонарних номерів, будь ласка, не забудьте ввести код міста.");
        list.add("Додаткова адреса веб-сайту компанії.");
        list.add("Додатковий опис діяльності.");
        list.add("Без цієї згоди не зможемо відправляти Вам, за допомогою електронної пошти, інформацію, наприклад про акції.");
        list.add("Прийняття правил дає право користуватися сайтом.");

        return list;
    }
}
