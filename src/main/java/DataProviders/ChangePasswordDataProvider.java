package DataProviders;

public class ChangePasswordDataProvider {

    public static String getExpectedValuePl = "Zmiana hasła - wpisz adres e-mail podany przy rejestracji.";
    public static String getExpectedValueUa = "Зміна пароля - введи адрес e-mail, який був вказаний під час реєстрації.";

    public static Object[][] getErrorMessage() {

        Object[][] objects = new String[2][2];
        objects[0][0] = "pl";
        objects[0][1] = "×\n" +
                "Błąd!\n" +
                "Podany adres e-mail nie istnieje w systemie lub konto konto użytkownika nie zostało jeszcze aktywowane.";

        objects[1][0] = "ua";
        objects[1][1] = "×\n" +
                "Помилка!\n" +
                "Вказаний адрес e-mail не існує в системі або профіль користувача ще не активований.";

        return objects;
    }
}
