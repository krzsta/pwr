package DataProviders;

import java.util.ArrayList;

public class EmployeeDataProvider {

    public static String getExpectedValuePl = "Rejestracja pracownika";
    public static String getExpectedValueUa = "Реєстрація працівника";

    public static ArrayList<String> getExpectedInformationValueStringForPl() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Interesująca Cię branża zawodowa.");
        list.add("Województwo, w którym szukasz pracy.");
        list.add("Bez tej zgody nie będziemy mogli przesyłać Państwu drogą mailową informacji na przykład o promocjach.");
        list.add("Akceptacja postanowień regulaminu uprawnia do korzystania z serwisu.");

        return list;
    }

    public static ArrayList<String> getExpectedInformationValueStringForUa() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Професія, яка тебе цікавить.");
        list.add("Область, в якій шукаєш роботу.");
        list.add("Без цієї згоди не зможемо відправляти Вам, за допомогою електронної пошти, інформацію, наприклад про акції.");
        list.add("Прийняття правил дає право користуватися сайтом.");

        return list;
    }

    public static ArrayList<String> getListStringForPl() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Nie podano adresu e-mail.");
        list.add("Nie wprowadzono hasła.");
        list.add("Nie wybrano żadnej branży.");
        list.add("Nie wybrano żadnego województwa.");
        list.add("Regulamin musi zostać zaakceptowany.");

        return list;
    }

    public static ArrayList<String> getListStringForUa() {

        ArrayList<String> list = new ArrayList<>();
        list.add("Не вказано адрес e-mail.");
        list.add("Не введений пароль.");
        list.add("Не вибрано жодної галузі.");
        list.add("Не вибрано жодної області.");
        list.add("Мусиш погодитись з Правилами сайту.");

        return list;
    }

    public static Object[][] getInvalidEmails() {

        String[] invalidEmails = {
                "plainaddress",
                "#@%^%#$@#$@#.com",
                "@example.com",
                "Joe Smith <email@example.com>",
                "email.example.com",
                "email@example@example.com",
                ".email@example.com",
                "email.@example.com",
                "email..email@example.com",
                "あいうえお@example.com",
                "email@example.com (Joe Smith)",
                "email@example",
                "email@-example.com",
                "email@111.222.333.44444",
                "email@example..com",
                "Abc..123@example.com"
        };

        Object[][] objects = new String[invalidEmails.length * 2][2];

        for (int i = 0; i < invalidEmails.length; i++) {
            objects[i][0] = "pl";
            objects[i][1] = invalidEmails[i];

            objects[i + invalidEmails.length][0] = "ua";
            objects[i + invalidEmails.length][1] = invalidEmails[i];
        }

        return objects;
    }
}
