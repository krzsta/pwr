package DataProviders;

import java.util.ArrayList;

public class SmallAdvertisementDataProvider {

    public static String getExpectedValuePl = "Dodawanie ogłoszenia";
    public static String getExpectedValueUa = "Додавання оголошення";

    public static ArrayList<String> getExpectedInformationValueStringForPl() {

        ArrayList<String> list = new ArrayList<>();
        list.add("W przypadku numerów stacjonarnych należy pamiętać o podaniu numeru kierunkowego.");

        return list;
    }

    public static ArrayList<String> getExpectedInformationValueStringForUa() {

        ArrayList<String> list = new ArrayList<>();
        list.add("У випадку стаціонарних номерів, будь ласка, не забудьте ввести код міста.");

        return list;
    }

    public static ArrayList<String> getListStringForPl() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Nie podano tytułu.");
        list.add("Nie podano telefonu kontaktowego.");
        list.add("Nie podano treści ogłoszenia.");

        return list;
    }

    public static ArrayList<String> getListStringForUa() {
        ArrayList<String> list = new ArrayList<>();
        list.add("Не вказано звання.");
        list.add("Не вказано контактний номер телефону.");
        list.add("Не заповнено зміст оголошення.");

        return list;
    }
}
