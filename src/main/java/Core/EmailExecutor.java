package Core;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailRequestInitializer;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static Helpers.Base64url.base64UrlDecode;
import static com.google.api.client.googleapis.javanet.GoogleNetHttpTransport.newTrustedTransport;
import static org.awaitility.Awaitility.await;

public class EmailExecutor {

    public static String properEmail = "";
    private static String emailAccountPassword = "";
    private static final String APPLICATION_NAME = "pwr";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String CREDENTIALS_FOLDER = "./src/main/resources/credentials";
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
    private static final String CLIENT_SECRET_DIR = "/client_secret.json";
    private static final String userId = "me";

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        InputStream in = EmailExecutor.class.getResourceAsStream(CLIENT_SECRET_DIR);
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(CREDENTIALS_FOLDER)))
                .setAccessType("offline")
                .build();
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize(userId);
    }

    private static Gmail getConnectionToGmailEmailAccount() {
        final NetHttpTransport HTTP_TRANSPORT;
        try {
            HTTP_TRANSPORT = newTrustedTransport();

            return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static ArrayList<String> getMessagesIdList() {
        Gmail gmail = getConnectionToGmailEmailAccount();
        ArrayList<String> messagesIdList = new ArrayList<>();

        try {
            ListMessagesResponse messagesResponse = gmail.users().messages().list(userId).execute();
            List<Message> messageList = messagesResponse.getMessages();

            try {
                messageList.forEach(message -> messagesIdList.add(message.getId()));
            } catch (Exception ignored) {
            }

            return messagesIdList;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteAllEmails() {
        ArrayList<String> emailsIdList = getMessagesIdList();
        Gmail gmail = getConnectionToGmailEmailAccount();

        emailsIdList.forEach(id -> {
            try {
                gmail.users().messages().trash(userId, id).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public static String getEmailBody() {
        getMessagesIdList();
        Gmail gmail = getConnectionToGmailEmailAccount();

        try {
            await().atMost(30, TimeUnit.SECONDS)
                    .pollDelay(2, TimeUnit.SECONDS)
                    .pollInterval(2, TimeUnit.SECONDS)
                    .until(() -> gmail.users().messages().get(userId, getMessagesIdList().get(0)).execute().size() > 0
                    );

            Message message = gmail.users().messages().get(userId, getMessagesIdList().get(0)).execute();
            String result = StringUtils.substringBetween(message.toString(), "},{\"body\":{\"data\":\"", "\",\"size\"");

            return base64UrlDecode(result);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
