package Core;

import java.sql.*;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static Core.Properties.*;
import static org.awaitility.Awaitility.await;

public class DbExecutor {

    public static ArrayList<String> getStringListDataFromSpecificColumnFromDatabase(String query, String columnLabel) {
        ArrayList<String> list = new ArrayList<>();

        try {
            Connection con = DriverManager.getConnection(pwrDbUrl, pwrDbUser, pwrDbPassword);
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(query);
            while (result.next()) {
                String title = result.getString(columnLabel);
                list.add(title);
            }
            con.close();

            return list;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void updateDatabase(String query) {
        try {
            Connection con = DriverManager.getConnection(pwrDbUrl, pwrDbUser, pwrDbPassword);
            Statement stmt = con.createStatement();
            stmt.executeQuery(query);
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void checkDatabaseForSpecificString(String query, String columnLabel, String expectedValue) {

        await().atMost(30, TimeUnit.SECONDS)
                .pollDelay(2, TimeUnit.SECONDS)
                .pollInterval(2, TimeUnit.SECONDS)
                .until(() -> getStringListDataFromSpecificColumnFromDatabase(query, columnLabel).contains(expectedValue)
                );
    }
}
