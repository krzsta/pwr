package Pages.PagesBeforeLogin;

import Pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginPopUpPage extends BasePage {

    public LoginPopUpPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "loginModal")
    private WebElement popUpLoginContainer;

    @FindBy(xpath = "//*[@id='loginModal']/form/div[1]/button")
    private WebElement closeButton;

    @FindBy(xpath = "//*[@id='loginModal']/form/div[2]/div/div[3]/div/div/a")
    private WebElement changePasswordLink;

    @FindBy(id = "remember")
    private WebElement rememberCheckbox;

    @FindBy(xpath = "//*[@id='loginModal']/form/div[3]/div[2]/a[1]")
    private WebElement userRegisterLink;

    @FindBy(xpath = "//*[@id='loginModal']/form/div[3]/div[2]/a[2]")
    private WebElement companyRegisterLink;

    @FindBy(xpath = "//*[@id='loginModal']/form/div[2]/div/div[1]")
    private WebElement errorMessage;

    @FindBy(id = "submit")
    private WebElement submitButton;

    public LoginPopUpPage clickOnCloseLoginPopUpPageButton() {
        closeButton.click();
        waitForElementInvisibility(closeButton);
        return this;
    }

    public EmployeeRegisterComponent clickOnUserRegisterLink() {
        userRegisterLink.click();
        return new EmployeeRegisterComponent(driver);
    }

    public EmployerRegisterComponent clickOnCompanyRegisterLink() {
        companyRegisterLink.click();
        return new EmployerRegisterComponent(driver);
    }

    public LoginPopUpPage checkRememberCheckbox() {
        rememberCheckbox.click();
        return this;
    }

    public ChangePasswordPage clickOnChangePasswordLink() {
        changePasswordLink.click();
        return new ChangePasswordPage(driver);
    }

    public LoginPopUpPage clickOnSubmitButton() {
        submitButton.click();
        return this;
    }

    public void assertThatErrorMessageAppears(String expectedValue) {
        waitForElementVisibility(errorMessage);
        Assert.assertEquals(errorMessage.getText(), expectedValue);
    }

    public void assertThatRememberIsChecked() {
        Assert.assertTrue(rememberCheckbox.isSelected());
    }

    public void assertThatRememberIsUnchecked() {
        Assert.assertFalse(rememberCheckbox.isSelected());
    }

    public void assertThatPopUpLoginComponentAppears() {
        waitForElementVisibility(popUpLoginContainer);
        Assert.assertTrue(popUpLoginContainer.isDisplayed());
    }

    public void assertThatLoginPopUpPageClosed() {
        Assert.assertFalse(popUpLoginContainer.isDisplayed());
    }
}
