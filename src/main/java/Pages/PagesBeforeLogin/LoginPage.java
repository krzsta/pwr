package Pages.PagesBeforeLogin;

import Pages.PagesAfterLogin.MainPageAfterLogin;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;

import static Core.DbExecutor.*;
import static Core.EmailExecutor.getEmailBody;

public class LoginPage extends MainPage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".alert.alert-success.template")
    private WebElement successAlertContainer;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(css = ".btn.btn-primary")
    private WebElement submitButton;

    public LoginPage fillEmailFieldAfterRegister(String email) {
        checkDatabaseForSpecificString("SELECT * FROM pracawreg_st.users", "email", email);
        updateDatabase("UPDATE users SET active = '1' WHERE email = '" + email + "'");
        updateDatabase("UPDATE users SET hash = '' WHERE email = '" + email + "'");
        emailField.sendKeys(email);
        return this;
    }

    public LoginPage fillPasswordField(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public MainPageAfterLogin submitButtonToLogin() {
        submitButton.click();
        return new MainPageAfterLogin(driver);
    }

    public void assertThatUserRegistered(String expectedValue) {
        waitForElementVisibility(successAlertContainer);
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/login"));
        Assert.assertEquals(successAlertContainer.getText(), expectedValue);
    }

    public void assertThatValidationEmailReceived(String email, ArrayList<String> expectedValue) {
        checkDatabaseForSpecificString("SELECT * FROM users", "email", email);
        String hash = getStringListDataFromSpecificColumnFromDatabase("SELECT * FROM users WHERE email = '" + email + "'", "hash").get(0);

        Assert.assertTrue(getEmailBody().contains(expectedValue.get(0)));
        Assert.assertTrue(getEmailBody().contains(expectedValue.get(1) + hash));
    }
}
