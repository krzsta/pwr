package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRegisterComponent extends MainPage {

    public EmployeeRegisterComponent(WebDriver driver) {
        super(driver);
    }

    public EmployeeRegisterComponent goToEmployeeRegisterPage(String lang) {
        String pageSuffix = "user/auth/registration";
        language(lang, pageSuffix);

        return this;
    }

    @FindBy(id = "registration")
    private WebElement registrationContainer;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "password_confirm")
    private WebElement confirmPasswordField;

    @FindBy(name = "gender")
    private WebElement genderSelect;

    @FindBy(name = "categories[]")
    private List<WebElement> categoriesCheckboxList;

    @FindBy(name = "voivodeships[]")
    private List<WebElement> voivodeshipsCheckboxList;

    @FindBy(name = "license_acceptance")
    private WebElement licenceCheckbox;

    @FindBy(name = "terms_of_use")
    private WebElement termOfUseCheckbox;

    @FindBy(css = "a[href*='/page/regulamin']")
    private WebElement regulationsLink;

    @FindBy(css = ".btn.btn-success.valid")
    private WebElement submitButton;

    @FindBy(css = "span.help-inline")
    private List<WebElement> errorMessagesList;

    @FindBy(xpath = "//*[@id='registration']/div[2]/div[5]/form//i")
    private List<WebElement> informationIconsList;

    @FindBy(css = ".tooltip.fade.left.in")
    private WebElement informationPopUp;

    public EmployeeRegisterComponent submitRegistrationForErrorMessages() {
        submitButton.click();
        return this;
    }

    public LoginPage submitRegistration() {
        submitButton.click();
        return new LoginPage(driver);
    }

    public EmployeeRegisterComponent fillEmailField(String email) {
        emailField.sendKeys(email);
        return this;
    }

    public EmployeeRegisterComponent fillPasswordField(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public EmployeeRegisterComponent fillConfirmPassword(String password) {
        confirmPasswordField.sendKeys(password);
        return this;
    }

    public EmployeeRegisterComponent chooseGender(String value) {
        Select select = new Select(genderSelect);
        select.selectByValue(value);
        return this;
    }

    public EmployeeRegisterComponent selectCategory(int value) {
        categoriesCheckboxList.get(value).click();
        return this;
    }

    public EmployeeRegisterComponent selectVoivodeship(int value) {
        voivodeshipsCheckboxList.get(value).click();
        return this;
    }

    public EmployeeRegisterComponent checkAllCategories() {
        categoriesCheckboxList.forEach(element -> element.click());
        return this;
    }

    public EmployeeRegisterComponent checkAllVoivodeships() {
        voivodeshipsCheckboxList.forEach(element -> element.click());
        return this;
    }

    public EmployeeRegisterComponent checkLicenceCheckbox() {
        licenceCheckbox.click();
        return this;
    }

    public EmployeeRegisterComponent checkTermOfUseCheckbox() {
        termOfUseCheckbox.click();
        return this;
    }

    public RegulationsComponent clikRegulationsLink() {
        regulationsLink.click();
        return new RegulationsComponent(driver);
    }

    public void AssertThatInformationPopUpDisplayed(List<String> expectedValue) {
        ArrayList<String> actualList = hoverOnElementAndGetText(informationPopUp);
        Assert.assertEquals(actualList, expectedValue);
    }

    private ArrayList<String> hoverOnElementAndGetText(WebElement element) {
        ArrayList<String> actualList = new ArrayList<>();

        for (WebElement anInformationIconsList : informationIconsList) {
            getAction().moveToElement(anInformationIconsList).build().perform();
            waitMilisec(50);
            actualList.add(element.getText());
        }

        return actualList;
    }

    public void assertThatTermOfUseCheckboxIsChecked() {
        Assert.assertTrue(termOfUseCheckbox.isSelected());
    }

    public void assertThatTermOfUseCheckboxIsUnchecked() {
        Assert.assertFalse(termOfUseCheckbox.isSelected());
    }

    public void assertThatLicenceCheckboxIsChecked() {
        Assert.assertTrue(licenceCheckbox.isSelected());
    }

    public void assertThatLicenceCheckboxIsUchecked() {
        Assert.assertFalse(licenceCheckbox.isSelected());
    }

    public void assertThatErrorMessagesAppears(List<String> expectedList) {
        ArrayList<String> actualList = new ArrayList<>();
        errorMessagesList.forEach(element -> actualList.add(element.getText()));
        Assert.assertEquals(expectedList, actualList);
    }

    public void assertThatGenderChanged(String expectedValue) {
        Select select = new Select(genderSelect);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), expectedValue);
    }

    public void assertThatCategoriesChecked() {
        categoriesCheckboxList.forEach(element -> Assert.assertTrue(element.isSelected()));
    }

    public void assertThatCategotiesUnchecked() {
        categoriesCheckboxList.forEach(element -> Assert.assertFalse(element.isSelected()));
    }

    public void assertThatVodeshipsChecked() {
        voivodeshipsCheckboxList.forEach(element -> Assert.assertTrue(element.isSelected()));
    }

    public void assertThatVodeshipsUnchecked() {
        voivodeshipsCheckboxList.forEach(element -> Assert.assertFalse(element.isSelected()));
    }

    public void assertThatPageOpened(String expectedValue) {
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/registration"));
        Assert.assertTrue(registrationContainer.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains(expectedValue));
    }

    public void assertThatProperEmailMessageAppears(String expectedMessage) {
        String actualMessage = errorMessagesList.get(0).getText();
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    public void assertThatProperPasswordErrorMessageAppears(String expectedMessage) {
        String actualMessage = errorMessagesList.get(1).getText();
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    public void assertThatConfirmPasswordErrorMessageAppears(String expectedMessage) {
        String actualMessage = errorMessagesList.get(2).getText();
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    public void assertThatUserNotRegistered() {
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/registration"));
        Assert.assertTrue(registrationContainer.isDisplayed());
    }
}
