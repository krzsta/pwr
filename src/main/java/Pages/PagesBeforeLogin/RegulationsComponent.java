package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class RegulationsComponent extends MainPage {

    public RegulationsComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "page")
    private WebElement regulationsContainer;

    public void assertThatRegulationsPageOpened(String expectedValue) {
        Assert.assertTrue(driver.getCurrentUrl().contains("/page/regulamin"));
        Assert.assertTrue(regulationsContainer.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains(expectedValue));
    }
}
