package Pages.PagesBeforeLogin;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ChangePasswordPage extends MainPage {

    public ChangePasswordPage(WebDriver driver) {
        super(driver);
    }

    public ChangePasswordPage goToChangePasswordPage(String lang) {
        String pageSuffix = "user/auth/change_password";
        language(lang, pageSuffix);

        return this;
    }

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(css = ".btn.btn-primary")
    private WebElement submitButton;

    @FindBy(css = ".alert.alert-danger.template")
    private WebElement errorMessageContainer;

    public ChangePasswordPage submitButtonForErrorMessage() {
        submitButton.click();
        return this;
    }

    public ChangePasswordPage clickOnCloseErrorMessageContainerButton() {
        waitForElementVisibility(errorMessageContainer);
        errorMessageContainer.findElement(By.className("close")).click();

        return this;
    }

    public void assertThatErrorMessageIsDisplayed(String expectedValue) {
        waitForElementVisibility(errorMessageContainer);

        Assert.assertEquals(expectedValue, errorMessageContainer.getText());
    }

    public void assertThatErrorMessageIsClosed() {
        try {
            Assert.assertTrue(errorMessageContainer.isDisplayed());
        } catch (ElementNotVisibleException | NoSuchElementException e) {
            Assert.assertFalse(false);
        }
    }

    public void assertThatPageOpened(String expectedValue) {
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/change_password"));
        Assert.assertTrue(emailField.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains(expectedValue));
    }
}
