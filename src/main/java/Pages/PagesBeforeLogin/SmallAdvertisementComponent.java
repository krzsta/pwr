package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class SmallAdvertisementComponent extends MainPage {

    public SmallAdvertisementComponent(WebDriver driver) {
        super(driver);
    }

    public SmallAdvertisementComponent goToSmallAdvertisementComponent(String lang) {
        String pageSuffix = "announcement/add";
        language(lang, pageSuffix);

        return this;
    }

    @FindBy(className = "col-md-12")
    private WebElement addAdvertisementContainer;

    @FindBy(xpath = "//*[@id='page-content']/div[2]//form//i")
    private List<WebElement> informationIconsList;

    @FindBy(css = ".tooltip.fade.left.in")
    private WebElement informationPopUp;

    @FindBy(css = "span.help-inline")
    private List<WebElement> errorMessagesList;

    @FindBy(css = ".btn.btn-success.valid")
    private WebElement submitButton;

    @FindBy(xpath = "//input[@name='title']")
    private WebElement titleField;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailField;

    @FindBy(id = "phone")
    private WebElement phoneField;

    @FindBy(name = "content")
    private WebElement contentField;

    public SmallAdvertisementComponent submitSmallAdvertisementForErrorMessages() {
        submitButton.click();
        return this;
    }

    private ArrayList<String> hoverOnElementAndGetText(WebElement element) {
        ArrayList<String> actualList = new ArrayList<>();

        for (WebElement anInformationIconsList : informationIconsList) {
            getAction().moveToElement(anInformationIconsList).build().perform();
            waitMilisec(50);
            actualList.add(element.getText());
        }

        return actualList;
    }

    public void AssertThatInformationPopUpDisplayed(List<String> expectedValue) {
        ArrayList<String> actualList = hoverOnElementAndGetText(informationPopUp);
        Assert.assertEquals(actualList, expectedValue);
    }

    public void assertThatPageOpened(String expectedValue) {
        Assert.assertTrue(driver.getCurrentUrl().contains("/announcement/add"));
        Assert.assertTrue(addAdvertisementContainer.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains(expectedValue));
    }

    public void assertThatErrorMessagesAppears(List<String> expectedList) {
        ArrayList<String> actualList = new ArrayList<>();
        errorMessagesList.forEach(element -> actualList.add(element.getText()));
        Assert.assertEquals(expectedList, actualList);
    }

    public SmallAdvertisementComponent fillTitleField(String title) {
        titleField.sendKeys(title);
        return this;
    }

    public SmallAdvertisementComponent fillEmailField(String email) {
        emailField.sendKeys(email);
        return this;
    }

    public SmallAdvertisementComponent fillPhoneNumberField(String phoneNumber) {
        phoneField.click();
        phoneField.sendKeys(phoneNumber);
        return this;
    }

    public SmallAdvertisementComponent fillContentField(String content) {
        contentField.sendKeys(content);
        return this;
    }

    public void assertThatNotSaveAdvertisement() {
        Assert.assertTrue(driver.getCurrentUrl().contains("/announcement/add"));
        Assert.assertTrue(addAdvertisementContainer.isDisplayed());
        Assert.assertTrue(errorMessagesList.get(0).isDisplayed());
    }

    public void assertThatCorrectErrorMessageAppears(String expectedMessage) {
        String actualMessage = errorMessagesList.get(0).getText();
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    public MainPage submitSmallAdvertisement() {
        submitButton.click();
        return new MainPage(driver);
    }
}
