package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopNavigationComponent extends MainPage {

    public TopNavigationComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "dropdownMenu")
    private WebElement dropdownMenu;

    @FindBy(className = "dropdown-menu")
    private WebElement dropdownMenuContainer;

    @FindBy(xpath = "//*[@id='header-navbar']//li[1]/a")
    private WebElement userRegisterLink;

    @FindBy(xpath = "//*[@id='header-navbar']//li[2]/a")
    private WebElement companyRegisterLink;

    @FindBy(xpath = "//*[@id='header-navbar']//li[4]/a")
    private WebElement loginLink;

    @FindBy(xpath = "//a[@href='/pl/']")
    private WebElement plLanguageSwitch;

    @FindBy(xpath = "//a[@href='/ua/']")
    private WebElement uaLanguageSwitch;

    @FindBy(id = "loginModal")
    private WebElement popUpLoginContainer;

    public TopNavigationComponent clickOnDropdownMenu() {
        dropdownMenu.click();
        waitForElementVisibility(dropdownMenuContainer);
        return this;
    }

    public EmployeeRegisterComponent clickOnUserRegisterLink() {
        userRegisterLink.click();
        return new EmployeeRegisterComponent(driver);
    }

    public EmployerRegisterComponent clickOnCompanyRegisterLink() {
        companyRegisterLink.click();
        return new EmployerRegisterComponent(driver);
    }

    public LoginPopUpPage clickOnLoginLink() {
        loginLink.click();
        waitForElementVisibility(popUpLoginContainer);
        return new LoginPopUpPage(driver);
    }

    public MainPage clickOnUkFlag() {
        uaLanguageSwitch.click();
        return this;
    }

    public MainPage clickOnPlFlag() {
        plLanguageSwitch.click();
        return this;
    }
}
