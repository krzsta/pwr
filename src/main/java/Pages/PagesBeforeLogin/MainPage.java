package Pages.PagesBeforeLogin;

import Pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static Core.DbExecutor.checkDatabaseForSpecificString;
import static Core.DbExecutor.updateDatabase;
import static Core.EmailExecutor.*;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".alert.alert-success.template")
    private WebElement successAlertContainer;

    @FindBy(id = "announcements")
    private WebElement announcementsContainer;

    public MainPage goToMainPage(String lang) {
        language(lang, "");
        return this;
    }

    public TopNavigationComponent getTopNavigationComponent() {
        return new TopNavigationComponent(driver);
    }

    public AddContainerComponent getAddContainerComponent() {
        return new AddContainerComponent(driver);
    }

    public void assertThatSwitchedToPolishLanguage() {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "https://dev:dev@www.stage.pracawregionie.pl/");
    }

    public void assertThatSwitchedToUkrainianLanguage() {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "https://dev:dev@www.stage.pracawregionie.pl/ua/");
    }

    public void assertThatAdvertisementAddedMessageAppears(String expectedMessage) {
        waitForElementVisibility(successAlertContainer);
        Assert.assertEquals(successAlertContainer.getText(), expectedMessage);
    }

    public void assertThatAdvertisementAppearsOnMainPage(String title) {
        checkDatabaseForSpecificString("SELECT * FROM announcements", "title", title);
        updateDatabase("UPDATE announcements SET accepted = '1' WHERE title = '" + title + "'");
        awaitForTextPresentInPageSource(title);

        Assert.assertTrue(announcementsContainer.findElement(By.xpath("//*[contains(text(), '" + title + "')]")).isDisplayed());
    }

    public void assertThatValidationEmailIsPresent(String title) {
        checkDatabaseForSpecificString("SELECT * FROM announcements", "title", title);
        Assert.assertTrue(getEmailBody().contains(title));
    }
}
