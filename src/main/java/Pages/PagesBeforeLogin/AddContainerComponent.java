package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddContainerComponent extends MainPage {

    public AddContainerComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[1]/a")
    private WebElement addCvLink;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    private WebElement addJobOfferLink;

    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[3]/a")
    private WebElement addSmallAdvertisement;

    public EmployeeRegisterComponent clickOnUserRegisterLink() {
        addCvLink.click();
        return new EmployeeRegisterComponent(driver);
    }

    public EmployerRegisterComponent clickOnCompanyRegisterLink() {
        addJobOfferLink.click();
        return new EmployerRegisterComponent(driver);
    }

    public SmallAdvertisementComponent clickOnSmallAdvertisementLink() {
        addSmallAdvertisement.click();
        return new SmallAdvertisementComponent(driver);
    }
}
