package Pages.PagesBeforeLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.ITestContext;

import java.util.ArrayList;
import java.util.List;

public class EmployerRegisterComponent extends MainPage {

    public EmployerRegisterComponent(WebDriver driver) {
        super(driver);
    }

    public EmployerRegisterComponent goToEmployerRegisterPage(String lang) {
        String pageSuffix = "user/auth/company_registration";
        language(lang, pageSuffix);

        return this;
    }

    @FindBy(id = "registration")
    private WebElement registrationContainer;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "password_confirm")
    private WebElement confirmPasswordField;

    @FindBy(name = "courtesy_form")
    private WebElement courtesyFormSelect;

    @FindBy(id = "position")
    private WebElement positionField;

    @FindBy(id = "name")
    private WebElement nameField;

    @FindBy(id = "surname")
    private WebElement surnameField;

    @FindBy(id = "phone")
    private WebElement phoneField;

    @FindBy(name = "recruitment_type_id")
    private WebElement recruitmentTypeSelect;

    @FindBy(id = "nip")
    private WebElement nipField;

    @FindBy(id = "regon")
    private WebElement regonField;

    @FindBy(id = "company_name")
    private WebElement companyNameField;

    @FindBy(id = "postal_code")
    private WebElement postalCodeField;

    @FindBy(id = "locality")
    private WebElement localityField;

    @FindBy(id = "address")
    private WebElement addressField;

    @FindBy(name = "voivodeship_id")
    private WebElement voivodeshipSelect;

    @FindBy(name = "category_id")
    private WebElement categorySelect;

    @FindBy(name = "employee_count")
    private WebElement employeeCountField;

    @FindBy(id = "company_email")
    private WebElement companyEmailField;

    @FindBy(id = "company_phone")
    private WebElement companyPhoneField;

    @FindBy(id = "www")
    private WebElement wwwField;

    @FindBy(id = "description")
    private WebElement descriptionField;

    @FindBy(name = "license_acceptance")
    private WebElement licenseAcceptanceCheckbox;

    @FindBy(name = "terms_of_use")
    private WebElement termsOfUseCheckbox;

    @FindBy(css = ".btn.btn-success.valid")
    private WebElement submitButton;

    @FindBy(css = "span.help-inline")
    private List<WebElement> errorMessagesList;

    @FindBy(css = "a[href*='/page/regulamin']")
    private WebElement regulationsLink;

    @FindBy(xpath = "//*[@id='registration']/div[2]/div/form//div/i")
    private List<WebElement> informationIconsList;

    @FindBy(css = ".tooltip.fade.left.in")
    private WebElement informationPopUp;

    @FindBy(tagName = "html")
    private WebElement html;

    public EmployerRegisterComponent submitRegistrationForErrorMessages() {
        submitButton.click();
        return this;
    }

    public RegulationsComponent clickRegulationsLink() {
        regulationsLink.click();
        return new RegulationsComponent(driver);
    }

    public EmployerRegisterComponent fillEmailField(String email) {
        emailField.sendKeys(email);
        return this;
    }

    public EmployerRegisterComponent fillPasswordField(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public EmployerRegisterComponent fillConfirmPassword(String password) {
        confirmPasswordField.sendKeys(password);
        return this;
    }

    public EmployerRegisterComponent chooseCourtesyForm(String value) {
        Select select = new Select(courtesyFormSelect);
        select.selectByValue(value);
        return this;
    }

    public EmployerRegisterComponent chooseRecruitmentType(String value) {
        Select select = new Select(recruitmentTypeSelect);
        select.selectByValue(value);
        return this;
    }

    public EmployerRegisterComponent chooseVoivodeshipType(String value) {
        Select select = new Select(voivodeshipSelect);
        select.selectByValue(value);
        return this;
    }

    public EmployerRegisterComponent chooseCategoryType(String value) {
        Select select = new Select(categorySelect);
        select.selectByValue(value);
        return this;
    }

    public EmployerRegisterComponent fillPositionField(String position) {
        positionField.sendKeys(position);
        return this;
    }

    public EmployerRegisterComponent fillNameField(String name) {
        nameField.sendKeys(name);
        return this;
    }

    public EmployerRegisterComponent fillSurnameField(String surname) {
        surnameField.sendKeys(surname);
        return this;
    }

    public EmployerRegisterComponent fillPhoneNumberField(String phoneNumber) {
        phoneField.click();
        phoneField.sendKeys(phoneNumber);
        return this;
    }

    public EmployerRegisterComponent fillNipField(String nip) {
        nipField.sendKeys(nip);
        return this;
    }

    public EmployerRegisterComponent fillRegonField(String regon) {
        regonField.sendKeys(regon);
        return this;
    }

    public EmployerRegisterComponent fillCompanyNameField(String companyName) {
        companyNameField.sendKeys(companyName);
        return this;
    }

    public EmployerRegisterComponent fillPostalCodeField(String postalCode) {
        postalCodeField.sendKeys(postalCode);
        return this;
    }

    public EmployerRegisterComponent fillLocalityField(String locality) {
        localityField.sendKeys(locality);
        return this;
    }

    public EmployerRegisterComponent fillAddressField(String address) {
        addressField.sendKeys(address);
        return this;
    }

    public EmployerRegisterComponent fillEmployeeCountField(String employeeCount) {
        employeeCountField.sendKeys(employeeCount);
        return this;
    }

    public EmployerRegisterComponent fillCompanyEmailField(String companyEmail) {
        companyEmailField.sendKeys(companyEmail);
        return this;
    }

    public EmployerRegisterComponent fillCompanyPhoneField(String companyPhoneNumber) {
        companyPhoneField.click();
        companyPhoneField.sendKeys(companyPhoneNumber);
        return this;
    }

    public EmployerRegisterComponent fillWebsiteField(String website) {
        wwwField.sendKeys(website);
        return this;
    }

    public EmployerRegisterComponent fillDescriptionField(String description) {
        descriptionField.sendKeys(description);
        return this;
    }

    public EmployerRegisterComponent checkTermOfUseCheckbox() {
        termsOfUseCheckbox.click();
        return this;
    }

    public EmployerRegisterComponent checkLicenceCheckbox() {
        licenseAcceptanceCheckbox.click();
        return this;
    }

    public EmployerRegisterComponent checkLicenseAcceptanceCheckbox() {
        licenseAcceptanceCheckbox.click();
        return this;
    }

    public EmployerRegisterComponent checkTermsOfUseCheckbox() {
        termsOfUseCheckbox.click();
        return this;
    }

    public void hoverAndAssertInformationIcons(List<String> expectedValue, ITestContext context) {
        if ("firefox".equals(context.getCurrentXmlTest().getParameter("browser"))) {
            pageZoomOut(5);
        }

        ArrayList<String> actualList = hoverOnElementAndAssertIsDisplayed(informationPopUp);
        Assert.assertEquals(actualList, expectedValue);
    }

    private ArrayList<String> hoverOnElementAndAssertIsDisplayed(WebElement element) {

        ArrayList<String> actualList = new ArrayList<>();

        for (WebElement anInformationIconsList : informationIconsList) {
            getAction().moveToElement(anInformationIconsList).build().perform();
            waitMilisec(50);
            actualList.add(element.getText());
        }
        return actualList;
    }

    public void assertThattermsOfUseCheckboxIsChecked() {
        Assert.assertTrue(termsOfUseCheckbox.isSelected());
    }

    public void assertThattermsOfUseCheckboxIsUnchecked() {
        Assert.assertFalse(termsOfUseCheckbox.isSelected());
    }

    public void assertThatLicenceCheckboxIsChecked() {
        Assert.assertTrue(licenseAcceptanceCheckbox.isSelected());
    }

    public void assertThatLicenceCheckboxIsUchecked() {
        Assert.assertFalse(licenseAcceptanceCheckbox.isSelected());
    }

    public void assertThatCategotyTypeChanged(String expectedValue) {
        Select select = new Select(categorySelect);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), expectedValue);
    }

    public void assertThatVoivodeshipTypeChanged(String expectedValue) {
        Select select = new Select(voivodeshipSelect);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), expectedValue);
    }

    public void assertThatRecruitmentTypeChanged(String expectedValue) {
        Select select = new Select(recruitmentTypeSelect);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), expectedValue);
    }

    public void assertThatCourtesyFormChanged(String expectedValue) {
        Select select = new Select(courtesyFormSelect);
        Assert.assertEquals(select.getFirstSelectedOption().getText(), expectedValue);
    }

    public void assertThatErrorMesagesAppears(ArrayList<String> expectedList) {
        ArrayList<String> actualList = new ArrayList<>();
        errorMessagesList.forEach(element -> actualList.add(element.getText()));
        Assert.assertEquals(actualList, expectedList);
    }

    public void assertThatPageOpened(String expectedValue) {
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/company_registration"));
        Assert.assertTrue(registrationContainer.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains(expectedValue));
    }

    public void assertThatUserNotRegistered() {
        Assert.assertTrue(driver.getCurrentUrl().contains("/user/auth/company_registration"));
        Assert.assertTrue(registrationContainer.isDisplayed());
    }

    public void assertThatCorrectErrorMessageAppears(String expectedMessage) {
        String actualMessage = errorMessagesList.get(0).getText();
        Assert.assertEquals(actualMessage, expectedMessage);
    }
}
