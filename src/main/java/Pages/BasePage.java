package Pages;

import Core.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class BasePage {
    protected WebDriver driver;
    private int timeOutInSeconds = 10;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    protected void language(String lang, String pageSuffix) {
        if ("pl".equals(lang.toLowerCase())) {
            driver.get(Properties.plApplicationPathUrl + pageSuffix);

        } else if ("ua".equals(lang.toLowerCase())) {
            driver.get(Properties.uaApplicationPathUrl + pageSuffix);
        }
    }

    protected static void pageZoomOut(int zoomOutLevel) {
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);

            for (int i = 0; i < zoomOutLevel; i++) {
                robot.keyPress(KeyEvent.VK_MINUS);
                robot.keyRelease(KeyEvent.VK_MINUS);
            }
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static void waitMilisec(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void waitForElementVisibility(WebElement element) {
        new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.visibilityOf(element));
    }

    protected void waitForElementInvisibility(WebElement element) {
        new WebDriverWait(driver, timeOutInSeconds).until(ExpectedConditions.invisibilityOf(element));
    }

    protected Actions getAction() {
        return new Actions(driver);
    }

    protected void awaitForTextPresentInPageSource(String text) {

        await().atMost(10, TimeUnit.SECONDS)
                .pollDelay(2, TimeUnit.SECONDS)
                .pollInterval(2, TimeUnit.SECONDS)
                .until(() -> refreshPageAndCheckTextIsInPageSource(text));
    }

    private boolean refreshPageAndCheckTextIsInPageSource(String text) {
        driver.navigate().refresh();
        return driver.getPageSource().contains(text);
    }
}
