package Helpers;

import org.apache.commons.codec.binary.Base64;

public class Base64url {

    public static String base64UrlDecode(String input) {
        Base64 decoder = new Base64(true);
        byte[] decodedBytes = decoder.decode(input);

        return new String(decodedBytes);
    }
}
