package Helpers;

import java.time.Instant;

public class DataTime {

    public static long getActualTimestamp() {
        Instant instant = Instant.now();
        return instant.toEpochMilli();
    }
}
